﻿-- P4. Baseado na massa de dados disponível no link, subscribers.zip, responda: 
--     Qual o tempo de vida médio de um cliente, com base na curva de sobrevivência?

-- consulta aplicando média aritimética
select 
    avg(cast(stop_date as date) - cast(start_date as date)) avg_days_on, 
    stddev(cast(stop_date as date) - cast(start_date as date)) as stddev
from subscribers where stop_date is not null;


--No aquivo p4.py é possível ver a implementação do código com a curva de sobrevivência de acordo com o método Kaplan Meier.