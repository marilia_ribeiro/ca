﻿-- P2. Escreva uma query SQL que responda a seguinte pergunta: 
--     Com qual frequência um caracter ocorre na primeira posição versus a segunda posição de uma string?

select c1.p1 as character, 
       c1.frequency_percent as p1_frequency_percent, 
       c2.frequency_percent as p2_frequency_percent
from
	(select substring(number_card,1,1) as p1, 
	       ((count(substring(number_card,1,1)) / (select count(*) from credit_card)::float) * 100) as frequency_percent
	from credit_card group by p1)  c1
left join
	(select substring(number_card,2,1) as p2, 
	       ((count(substring(number_card,2,1)) / (select count(*) from credit_card)::float) * 100) as frequency_percent
	from credit_card where substring(number_card,2,1) = substring(number_card,1,1) group by p2) c2
on (c1.p1=c2.p2)
