﻿-- P3. Considere que o mês atual é Dezembro/15 e o time de Marketing deseja fazer uma campanha no início do ano, 
--   mais específico em Janeiro/16, para os clientes que completam o 1º ano na base de assinantes. 
--   Para isso o time de Marketing precisa que você ajude a responder seguinte a pergunta: 
--   Quantos clientes atuais possuem o 1º aniversário de assinatura no próximo mês (janeiro). 
--   Monte a query SQL que responde a pergunta.


select count(*) as signature_anniversary from subscribers 
where concat(start_date) like '2015-01-%' and
(stop_date is null or concat(stop_date)  like '2016-01-%')