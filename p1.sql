﻿-- P1. Para identificar qual a bandeira de um cartão de crédito é usado os 6 primeiros números do cartão de crédito, chamado de bin number.
--     Em uma base de dados, por razão de segurança, se persiste apenas os 6 primeiros dígitos do cartão de um cliente. 
--     Escreva uma query SQL que permita identificar a bandeira com base na tabela abaixo:

--     American Express=34, 37
--     Diners Club=300-305, 309, 36, 38, 39,54, 55
--     Mastercard=51-55
--     Visa=4
--     Outros

create table credit_card(
    id serial not null,
    flag_card varchar(100),
    number_card varchar(6)
 );

insert into credit_card (id, flag_card, number_card) values	
(1, 'American Express', '342346'),
(2, 'American Express', '343467'),
(3, 'American Express', '342783'),
(4, 'Diners Club', '368982'),
(5, 'Diners Club', '365226'),
(6, 'Diners Club', '365476'),
(7, 'Diners Club', '541615'),
(8, 'Diners Club', '542553'),
(9, 'Diners Club', '544687'),
(10, 'Mastercard', '518657'),
(11, 'Mastercard', '519545'),
(12, 'Mastercard', '511695'),
(13, 'Visa', '471464'),
(14, 'Visa', '424987'),
(15, 'Visa', '483629'),
(16, 'American Express', '372783'),
(17, 'Diners Club', '300892'),
(18, 'Diners Club', '301892'),
(19, 'Diners Club', '302898'),
(20, 'Diners Club', '303892'),
(21, 'Diners Club', '304892'),
(22, 'Diners Club', '305892'),
(23, 'Diners Club', '388882'),
(24, 'Diners Club', '390892'),
(25, 'Diners Club', '540892'),
(26, 'Diners Club', '550882'),
(27, 'Mastercard', '521695'),
(28, 'Mastercard', '531695'),
(29, 'Mastercard', '541695'),
(30, 'Mastercard', '541695'),
(31, 'Outros', '941695');
(32, 'Mastercard', '551695');


select *, 
    case 
        when left(number_card,2)  in ('34','37') then 'American Express'
        when (left(number_card,2) in ('36', '38', '39')) or (cast(left(number_card,3) as int) between 300 and 305) then 'Diners Club'
        when (cast(left(number_card,2) as int) between 51 and 53) then 'Mastercard'
        when left(number_card,2) in ('54', '55') then 'Diners Club ou Mastercard'
        when left(number_card,1) = '4' then 'Visa'       
        else 'Outros'
    end as group
from credit_card;