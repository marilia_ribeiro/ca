import csv

import pendulum
from lifelines import KaplanMeierFitter

FILE_PATH = 'Subscribers.txt'

with open(FILE_PATH, 'r') as f:
    content = csv.reader(f, delimiter='\t')
    next(content)
    durations = []
    event_observed = []
    for row in content:
        censored = bool(row[9])
        start_date = pendulum.from_format(row[5], 'YYYY-MM-DD') if row[5] and row[5] != 'NULL' else None
        stop_date = pendulum.from_format(row[6], 'YYYY-MM-DD') if row[6] and row[6] != 'NULL' else None
        if start_date and stop_date:
            duration = (stop_date - start_date).days
            durations.append(duration)
            event_observed.append(censored)

    kmf = KaplanMeierFitter()
    kmf.fit(durations, event_observed)

    median = kmf.median_survival_time_
    median_survival_time = kmf.median_survival_time_
    print('median: {}   median_survival_time: {}'.format(median, median_survival_time))
    # median: 425.0   median_survival_time: 425.0

