P6. Conte o que você descobre sobre os dados no arquivo veiculos.zip. A descrição das variáveis coletadas são:

Foi utilizado o software eduacional weka para realilzar a tarefa de agrupamento dos dados.
Para a análise foram utilizados os algorítmos EM e Simple Kmeans. As variáveis combinadas foram modelo, opcionais, design e geral.

## Os resultados com o EM foram:

Utilizando o atributo `geral` como classe foi possível encontrar 4 grupos. 

 - No primeiIo grupo, com **35%**, estão os `Muito insatisfeito`, sendo a predominante carros do modelo `Chiconalta`, com opcionais `Inexistentes`, e desing `Adiante dos outros`.
 - No segrundo grupo, com **28%**, estã os `Insatisfeito`, sendo a predominante carros do modelo `Deltaforce3`, com opcionais `Ar_e_direção` ou `Inexistentes`, e desing `Atualizados`.
- No terceiro grupo, com **25%**, estão os `Satisfeito`, sendo a predominante carros do modelo `Valentina`, com opcionais `AD_Trio_Elétrico`, e desing `Adiante dos outros`.
- No quarto grupo, com **13%**, estão os `Bastante satisfeito`, sendo a predominante carros do modelo `Chiconalta`, com opcionais `Inexistentes`, e desing `Adiante dos outros`.

## Os resultados com o Simple Kmenas foram:

Utilizando a opção training set como classe foi possível encontrar 2 grupos. 

 - No primeiro grupo, com **67%**, estão os `Insatisfeito`, sendo a predominante carros do modelo `Deltaforce3`, com opcionais `Ar_e_direção`, e desing `Atualizados`.
 - No segundo grupo, com **33%**, estão os `Muito insatisfeito`, sendo a predominante carros do modelo `Chiconalta`, com opcionais `Inexistentes`, e desing `Adiante dos outros`.
