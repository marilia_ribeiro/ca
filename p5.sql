﻿-- P5. Efetue uma análise RFM baseado nos dados disponível no link, orders_customers.zip.

-- | Escala R (order_date) | Escala F (num_units) | Escala M (total_price) |
-- | 1 se 365 < x 	   | 1 se x = 0		  | 1 se x <= 25           |
-- | 2 se 180 < x <= 365   | 2 se x <= 5	  | 2 se 25 < x <= 50	   |
-- | 3 se 30 < x <= 180	   | 3 se 5 < x <= 10	  | 3 se 50< x <= 100	   |
-- | 4 se 15 < x <= 30	   | 4 se 10 < x <= 20	  | 4 se 100 < x <= 300	   |
-- | 5 se  x <= 15	   | 5 se 20 < x	  | 5 se 300 < x	   |

-- rfm = (r * 5) + (f * 5) + (m * 5)

select
	o.customer_id as customer_id,
	c.firstname as firstname,
	o.rfm as rfm
from
	(select
	   customer_id,
	   (((case
		when (cast('2016-09-20' as date) - cast(max(order_date) as date)) > 365 then 1
		when (cast('2016-09-20' as date) - cast(max(order_date) as date)) > 180 and (cast('2016-09-20' as date) - cast(max(order_date) as date)) <= 365 then 2
		when (cast('2016-09-20' as date) - cast(max(order_date) as date)) > 30 and (cast('2016-09-20' as date) - cast(max(order_date) as date)) <= 180 then 3
		when (cast('2016-09-20' as date) - cast(max(order_date) as date)) > 15 and (cast('2016-09-20' as date) - cast(max(order_date) as date)) <= 30 then 4
		when (cast('2016-09-20' as date) - cast(max(order_date) as date)) <= 15 then 5
	   end) * 5) +
	   ((case
		when sum(num_units) = 0 or sum(num_units) is null then 1
		when sum(num_units) <= 5 then 2
		when sum(num_units) > 5 and sum(num_units) <= 10 then 3
		when sum(num_units) > 10 and sum(num_units) <= 20 then 4
		when sum(num_units) > 20 then 5	
	   end) * 5) +
	   ((case
		when sum(total_price) <= 25 then 1
		when sum(total_price) > 25 and sum(total_price) <= 50 then 1
		when sum(total_price) > 50 and sum(total_price) <= 100 then 1
		when sum(total_price) > 100 and sum(total_price) <= 300 then 1
		when sum(total_price) > 300 then 1
	   end) * 5)) as rfm
	from
	   orders
	where 
	   cast(order_date as date) <= cast('2016-09-20' as date) and  
	   cast(order_date as date) >= cast('2015-01-01' as date)
	group by customer_id
	order by customer_id limit 100) o
left join
	(select customer_id, firstname from customers) c
on (o.customer_id=c.customer_id);
	


-- O resultado da análise se encontra no arquivo rfm_result.csv
-- Os três potenciais clientes são:

-- |  ID |    NAME   | RFM |
-- | 0	 |		     | 55  |
-- | 154 |	CAROLINE | 40  |                                                                                                                                                                                                                                              	40
-- | 152 |	CAROLINE | 35  |                                                                                                                                                                                                                                                  	35
