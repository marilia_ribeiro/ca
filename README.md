# Desafio técnico

Inicialmente você precisará baixar os arquivos necessários no link abaixo:

https://drive.google.com/folderview?id=0BybH-wfM2e7VZnIwb0V1WUtWU2s&usp=sharing



**P1.** Para identificar qual a bandeira de um cartão de crédito é usado os 6 primeiros números do cartão de crédito, chamado de bin number. Em uma base de dados, por razão de segurança, se persiste apenas os 6 primeiros dígitos do cartão de um cliente. Escreva uma query SQL que permita identificar a bandeira com base na tabela abaixo:
- American Express=34, 37
- Diners Club=300-305, 309, 36, 38, 39,54, 55
- Mastercard=51-55
- Visa=4
- Outros

**P2.** Escreva uma query SQL que responda a seguinte pergunta: Com qual frequência um caracter ocorre na primeira posição versus a segunda posição de uma string?

**P3.** Considere que o mês atual é Dezembro/15 e o time de Marketing deseja fazer uma campanha no início do ano, mais específico em Janeiro/16, para os clientes que completam o 1º ano na base de assinantes. Para isso o time de Marketing precisa que você ajude a responder seguinte a pergunta: Quantos clientes atuais possuem o 1º aniversário de assinatura no próximo mês (janeiro). Monte a query SQL que responde a pergunta.

**P4.** Baseado na massa de dados disponível no link, subscribers.zip, responda: Qual o tempo de vida médio de um cliente, com base na curva de sobrevivência?

**P5.** Efetue uma análise RFM baseado nos dados disponível no link, orders_customers.zip.

**P6.** Conte o que você descobre sobre os dados no arquivo veiculos.zip. A descrição das variáveis coletadas são:
- Modelo comprado: o compacto Chiconaultla, o sedã médio DeltaForce3, a perua familiar Valentiniana, a van SpaceShuttle ou o luxuoso LuxuriousCar.
- Opcionais: inexistentes (apenas os itens de série); ar-condicionado e direção hidráulica; ar-condicionado, direção hidráulica e trio elétrico; ar-condicionado, direção hidráulica, trio elétrico e freios ABS.
- Opinião sobre o design: se os clientes consideram o design do veículo comprado ultrapassado, atualizado, ou adiante dos concorrentes.
- Opinião sobre a concessionária onde comprou o veículo (incluindo atendimento na venda, manutenção programada e eventuais problemas imprevistos): muito insatisfatória, insatisfatória, não causou impressão, satisfatória, bastante satisfatória.
- Opinião geral sobre o veículo adquirido: muito insatisfeito, insatisfeito, satisfeito, bastante satisfeito.
- Renda declarada pelo cliente: em salários mínimos mensais.
- Número de pessoas geralmente transportadas no veículo.
- Quilometragem mensal média percorrida com o veículo.
- Percepção do cliente de há quantos anos o veículo comprado teve a sua última remodelação de design: em anos completos (se há menos de um ano o entrevistador anotou zero).
- Idade do cliente em anos completos.
